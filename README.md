# Wardrobify

Team:

* Lauren Mealer - Hats
* Alyson Golden - Shoes

## Design

![alt text](.excalidraw.png)

## Shoes microservice

I took the model for the Bin in in Wardrobe api and looked at the characteristics of the model. I found that what would be needed for the model to be successful in my code would be to bring over 2 of the 3 arguments, the closet_name and the bin_number. This would allow definition and sorting of the bins in the correct areas. (While bin_size is important it is not necessary in the act of locating and tracking shoes.) I incorporated those arguments into my BinVO within the Shoes microservice. Afterwards I used a forigen key to connect the Shoe model to the BinVO.

## Hats microservice

The hats microservice is responsible for all the hats within the app (adding, changing, deleting hats), and location of the hat in the wardrobe. the wardrobe microservice handles the closet name, section number and shelf number. they both relate through the location info from wardrobe to each hat from the hat microservice. when i add/del/change a hat, the wardrobe microservice would reflect those changes as well. my locationvo model relates to the wardrobe model Location where the location info is stored and used anytime i make changes or view an item in the hat microservice.
