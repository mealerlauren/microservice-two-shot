import React from 'react'; // Ensure you import React for JSX to work
import './index.css';


function MainPage() {
  return (
    <><div className="container-fluid"> {/* Fluid container for full width */}
      <header className="py-5 bg-light border-bottom mb-4">
        <div className="container">
          <h1 className="display-4 fw-bold text-center">WARDROBIFY!</h1>
          <p className="lead text-center">Your Closet Organizing Solution</p>

          <div className="text-center">  <h3>Easy Storage</h3>
            <p>Quickly add and categorize your shoes and hats.</p>
          </div>
        </div>
      </header>
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8">
          </div>
          <div id="carouselExampleInterval" className="carousel slide" data-bs-ride="carousel">
            <div className="carousel-inner">
              <div className="carousel-item active" data-bs-interval="3000">
                <img src="https://images.urbndata.com/is/image/Anthropologie/84758077_010_b?$a15-pdp-detail-shot$&fit=constrain&fmt=webp&qlt=80&wid=1080" className="d-block mx-auto" alt="..." style={{ maxWidth: '50%', height: '50%' }}></img>
              </div>
              <div className="carousel-item" data-bs-interval="3000">
                <img src="https://images.urbndata.com/is/image/Anthropologie/89104715_085_b?$a15-pdp-detail-shot$&fit=constrain&fmt=webp&qlt=80&wid=960" className="d-block mx-auto" alt="..." style={{ maxWidth: '50%', height: '50%' }}></img>
              </div>
              <div className="carousel-item" data-bs-interval="3000">
                <img src="https://images.urbndata.com/is/image/Anthropologie/89945513_020_b?$a15-pdp-detail-shot$&fit=constrain&fmt=webp&qlt=80&wid=1080" className="d-block mx-auto" alt="..." style={{ maxWidth: '50%', height: '50%' }}></img>
              </div>
              <div className="carousel-item" data-bs-interval="3000">
                <img src="https://images.urbndata.com/is/image/Anthropologie/89125041_014_b?$a15-pdp-detail-shot$&fit=constrain&fmt=webp&qlt=80&wid=960" className="d-block mx-auto" alt="..." style={{ maxWidth: '50%', height: '50%' }}></img>
              </div>
              <div className="carousel-item" data-bs-interval="3000">
                <img src="https://images.urbndata.com/is/image/Anthropologie/88062310_070_b?$a15-pdp-detail-shot$&fit=constrain&fmt=webp&qlt=80&wid=1080" className="d-block mx-auto" alt="..." style={{ maxWidth: '50%', height: '50%' }}></img>
              </div>
              <div className="carousel-item" data-bs-interval="3000">
                <img src="https://images.urbndata.com/is/image/Anthropologie/84504968_023_b?$a15-pdp-detail-shot$&fit=constrain&fmt=webp&qlt=80&wid=960" className="d-block mx-auto" alt="..." style={{ maxWidth: '50%', height: '50%' }}></img>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><main className="container">
        {/* Feature Section */}
        <div className="row my-5">

          {/* Add more col-md-4 feature blocks as needed */}

        </div>
      </main></>
  );
}

export default MainPage;
