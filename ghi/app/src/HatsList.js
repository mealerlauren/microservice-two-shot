import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import React from 'react';

function HatsCard(props) {
  const [deleted, setDeleted] = React.useState(false);

  const hats = props.hats;

  const handleDelete = async () => {
    try {
      const url = `http://localhost:8090/api/hats/${hats.id}`;
      const response = await fetch(url, { method: 'DELETE' });

      if (response.ok) {
        console.log("Hat successfully deleted!");

        setDeleted(true);
        // Reload the page
        window.location.reload();
      } else {
        console.error('Delete failed:', response);
      }
    } catch (error) {
      console.error('Error during deletion:', error);
    }
  }

  // Render nothing if the hat is deleted
  if (deleted) {
    return null;
  }

  return (
    <div key={hats.id} className="card mb-3 shadow p-2">
      <img src={hats.picture_url} className="card-img-top" />
    <div className="card-body">
    <ul className="list-group">
          <li className="list-group-item">Fabric: {hats.fabric}</li>
          <li className="list-group-item">Style: {hats.style_name}</li>
          <li className="list-group-item">Color: {hats.color}</li>
          <li className="list-group-item">Closet: {hats.location.name}</li>
          <li className="list-group-item">Section: {hats.location.section_number}</li>
          <li className="list-group-item">Shelf: {hats.location.shelf_number}</li>
        </ul>
        <div className="text-center">
          <button onClick={handleDelete} className="btn-outline-secondary">Delete</button>
        </div>
    </div>
  </div>
  );
}


function HatsColumn(props) {
  return (
    <div className="col">
      {props.list.map((hats, index) => {
        return <HatsCard key={index} hats={hats} />;
      })}
    </div>
  );
}

const HatsList = () => {
  const [hatsColumns, setHatsColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = "http://localhost:8090/api/hats/";
    try {
      const response = await fetch(url);

      const columns = [[], [], []];
      if (response.ok) {
        const data = await response.json();
        let index = 0;
        for (const hat of data.hats) {
          columns[index].push(hat);
          index += 1;
          if (index > 2) {
            index = 0;
          }
        }
      }
      setHatsColumns(columns);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="row">
        <div className="header-card" element="col d-flex justify-content-center">
          <div className="card text-center shadow-light mb-3">
            <h2 className="card-title">Hats</h2>
            <a className="btn-outline-secondary" href="/hats/new/" role="button">Add hat</a>

          </div>
        </div>
      </div>
      <div className="row">
        {hatsColumns.map((hatsColumnList, index) => {
          return <HatsColumn key={index} list={hatsColumnList} />;
        })}
      </div>
    </div>
  );
};
export default HatsList;
